package cz.msik.sapho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaphoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaphoApplication.class, args);
    }
}

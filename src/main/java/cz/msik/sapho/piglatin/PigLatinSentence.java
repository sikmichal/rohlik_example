package cz.msik.sapho.piglatin;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
class PigLatinSentence {
    private String sentence;
}

package cz.msik.sapho.piglatin;

import org.hibernate.validator.constraints.NotEmpty;

public interface PigLatinService {
    /**
     * Write some Java code that translates a string (word, sentence, or paragraph) into “pig-latin” using the following rules.
     * Words that start with a consonant have their first letter moved to the end of the word and the letters “ay” added to the end.
     * Hello becomes Ellohay
     * <p>
     * Words that start with a vowel have the letters “way” added to the end.
     * apple becomes appleway
     * <p>
     * Words that end in “way” are not modified.
     * stairway stays as stairway
     * <p>
     * Punctuation must remain in the same relative place from the end of the word.
     * can’t becomes antca’y
     * end. becomes endway.
     * <p>
     * Hyphens are treated as two words
     * this-thing becomes histay-hingtay
     * <p>
     * Capitalization must remain in the same place.
     * Beach becomes Eachbay
     * McCloud becomes CcLoudmay
     */
    PigLatinSentence transformToPigLatin(@NotEmpty String input);
}

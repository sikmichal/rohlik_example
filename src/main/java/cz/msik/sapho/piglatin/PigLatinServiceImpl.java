package cz.msik.sapho.piglatin;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Validated
public class PigLatinServiceImpl implements PigLatinService {
    @Value("${pigLatin.wordDefinitionPattern}")
    private String wordDefinitionPattern;
    @Value("${pigLatin.consonantsPattern}")
    private String consonantsPattern;
    @Value("${pigLatin.consonantsAdditionalSuffix}")
    private String consonantsAdditionalSuffix;
    @Value("${pigLatin.vowelsPattern}")
    private String vowelsPattern;
    @Value("${pigLatin.vowelsAdditionalSuffix}")
    private String vowelsAdditionalSuffix;
    @Value("${pigLatin.keepWithoutChangePattern}")
    private String keepWithoutChangePattern;
    @Value("${pigLatin.punctuationPattern}")
    private String punctuationPattern;

    private static final String EMPTY_STRING = "";

    @Override
    public PigLatinSentence transformToPigLatin(@NotEmpty String input) {
        StringBuffer pigLatinSentence = new StringBuffer();
        Matcher matcher = Pattern.compile(wordDefinitionPattern).matcher(input);
        while (matcher.find()) {
            matcher.appendReplacement(
                    pigLatinSentence,
                    getPigLatinWord(matcher.group(1))
            );
        }
        return new PigLatinSentence(pigLatinSentence.toString());
    }

    private String getPigLatinWord(final String word) {
        String wordWithoutPunctuations = getWordWithoutPunctuations(word);
        String pigLatinTransformedWord = getWordWithAppliedPigLatinRules(wordWithoutPunctuations);
        return getWordEnhancedWithPunctuations(pigLatinTransformedWord, getPunctuationsFromWord(word));
    }

    private String getWordWithoutPunctuations(final String word) {
        return word.replaceAll(punctuationPattern, EMPTY_STRING);
    }

    private String getWordWithAppliedPigLatinRules(final String word) {
        if (isWordEndingWithNoChangeLetters(word)) {
            return word;
        } else if (isWordStartingWithConsonant(word)) {
            return moveConsonantFromBeggingToEndAndAddSuffixLetters(word);
        } else if (isWordStartingWithVowel(word)) {
            return addAdditionalLettersToEndOfVowelStartingWords(word);
        } else {
            return word;
        }
    }

    private boolean isWordEndingWithNoChangeLetters(final String word) {
        return Pattern.compile(keepWithoutChangePattern, Pattern.CASE_INSENSITIVE).matcher(word).find();
    }

    private boolean isWordStartingWithConsonant(final String word) {
        return Pattern.compile(consonantsPattern, Pattern.CASE_INSENSITIVE).matcher(word).find();
    }

    private boolean isWordStartingWithVowel(final String word) {
        return Pattern.compile(vowelsPattern, Pattern.CASE_INSENSITIVE).matcher(word).find();
    }

    private List<Punctuation> getPunctuationsFromWord(final String word) {
        List<Punctuation> punctuations = new ArrayList<>();
        Matcher matcher = Pattern.compile(punctuationPattern).matcher(word);
        while (matcher.find()) {
            punctuations.add(new Punctuation(
                    getSubstringIndexFromEndOfWord(word, matcher.start()),
                    matcher.group()
            ));
        }
        return punctuations;
    }

    private int getSubstringIndexFromEndOfWord(final String word, int indexFromStart) {
        return word.length() - indexFromStart - 1;
    }

    private String getWordEnhancedWithPunctuations(String pigLatinWord, List<Punctuation> punctuations) {
        StringBuilder pigLatinWordBuilder = new StringBuilder(pigLatinWord);
        punctuations.forEach(punctuation -> pigLatinWordBuilder.insert(
                getStartIndexInWordBasedOnRelativeIndexFromEnd(pigLatinWordBuilder, punctuation.getRelativeIndexFromEnd()),
                punctuation.getPunctuation())
        );
        return pigLatinWordBuilder.toString();
    }

    private int getStartIndexInWordBasedOnRelativeIndexFromEnd(StringBuilder pigLatinWord, int indexFromEnd) {
        return pigLatinWord.length() - indexFromEnd;
    }

    private String moveConsonantFromBeggingToEndAndAddSuffixLetters(final String word) {
        StringBuilder pigLatinWord = new StringBuilder();
        pigLatinWord.append(word.substring(1).toLowerCase());
        pigLatinWord.append(word.substring(0, 1).toLowerCase());
        pigLatinWord.append(consonantsAdditionalSuffix);

        return getRecapitalizedWord(word, pigLatinWord);
    }

    private String getRecapitalizedWord(final String word, StringBuilder pigLatinWord) {
        Pattern pattern = Pattern.compile("[A-Z]");
        Matcher matcher = pattern.matcher(word);
        while (matcher.find()) {
            pigLatinWord.setCharAt(matcher.start(), Character.toUpperCase(pigLatinWord.charAt(matcher.start())));
        }
        return pigLatinWord.toString();
    }

    private String addAdditionalLettersToEndOfVowelStartingWords(final String word) {
        return word + vowelsAdditionalSuffix;
    }
}
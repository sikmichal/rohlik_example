package cz.msik.sapho.piglatin;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class Punctuation {
    private int relativeIndexFromEnd;
    private String punctuation;
}

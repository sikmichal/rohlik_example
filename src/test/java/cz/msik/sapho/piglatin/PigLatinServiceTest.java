package cz.msik.sapho.piglatin;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;

/**
 * {@link PigLatinServiceImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PigLatinServiceTest {
    @Autowired
    private PigLatinService pigLatinService;

    @Test(expected = ConstraintViolationException.class)
    public void shouldFailWithNullInputValue() {
        pigLatinService.transformToPigLatin(null);
    }

    @Test(expected = ConstraintViolationException.class)
    public void shouldFailWithEmptyInputValue() {
        pigLatinService.transformToPigLatin("");
    }

    @Test
    public void shouldMoveConsonantFromBeggingToEndAndAddAyLetters() {
        Assert.assertEquals(new PigLatinSentence("Ellohay"), pigLatinService.transformToPigLatin("Hello"));
        Assert.assertEquals(new PigLatinSentence("tairSay"), pigLatinService.transformToPigLatin("staiR"));
        Assert.assertEquals(new PigLatinSentence("fay"), pigLatinService.transformToPigLatin("f"));
    }

    @Test
    public void shouldAddWayLettersToEndOfVowelStartingWords() {
        Assert.assertEquals(new PigLatinSentence("appleway"), pigLatinService.transformToPigLatin("apple"));
        Assert.assertEquals(new PigLatinSentence("epicway"), pigLatinService.transformToPigLatin("epic"));
        Assert.assertEquals(new PigLatinSentence("away"), pigLatinService.transformToPigLatin("a"));
    }

    @Test
    public void shouldNotModifyWayLettersEndingWords() {
        Assert.assertEquals(new PigLatinSentence("stairway"), pigLatinService.transformToPigLatin("stairway"));
        Assert.assertEquals(new PigLatinSentence("airWay"), pigLatinService.transformToPigLatin("airWay"));
        Assert.assertEquals(new PigLatinSentence("airWay."), pigLatinService.transformToPigLatin("airWay."));
    }

    @Test
    public void shouldNotModifyWordsWithWayLettersAndPunctuationInThem() {
        Assert.assertEquals(new PigLatinSentence("stairw:ay"), pigLatinService.transformToPigLatin("stairw:ay"));
        Assert.assertEquals(new PigLatinSentence("airWa'y"), pigLatinService.transformToPigLatin("airWa'y"));
    }

    @Test
    public void shouldKeepPunctuationInTheSameRelativePlace() {
        Assert.assertEquals(new PigLatinSentence("antca:y"), pigLatinService.transformToPigLatin("can:t"));
        Assert.assertEquals(new PigLatinSentence("antca'y"), pigLatinService.transformToPigLatin("can't"));
        Assert.assertEquals(new PigLatinSentence("endway."), pigLatinService.transformToPigLatin("end."));
        Assert.assertEquals(new PigLatinSentence("'ahoyway: aptaincay'"), pigLatinService.transformToPigLatin("'ahoy: captain'"));
    }

    @Test
    public void shouldProcessHyphensAsTwoWords() {
        Assert.assertEquals(new PigLatinSentence("histay-hingtay"), pigLatinService.transformToPigLatin("this-thing"));
        Assert.assertEquals(new PigLatinSentence("enDway.-hingtay"), pigLatinService.transformToPigLatin("enD.-thing"));
        Assert.assertEquals(new PigLatinSentence("histay--stairway"), pigLatinService.transformToPigLatin("this--stairway"));
        Assert.assertEquals(new PigLatinSentence("stairway- -airway"), pigLatinService.transformToPigLatin("stairway- -airway"));
    }

    @Test
    public void shouldKeepCapitalizationInTheSamePlace() {
        Assert.assertEquals(new PigLatinSentence("Eachbay"), pigLatinService.transformToPigLatin("Beach"));
        Assert.assertEquals(new PigLatinSentence("CcLoudmay"), pigLatinService.transformToPigLatin("McCloud"));
    }

    @Test
    public void shouldTranslateSentenceToPigLatinSentence() {
        Assert.assertEquals(new PigLatinSentence("Epicway APPleway ayssay 'elloha,y' otay Inkytay-Inkyway onway ishay way otay stairway otay airway."),
                pigLatinService.transformToPigLatin("Epic APPle says 'hell,o' to Tinky-Winky on his way to stairway to airway."));
    }

    @Test
    public void shouldTranslateSentenceToPigLatinSentenceWithSpecialChars() {
        Assert.assertEquals(new PigLatinSentence("Epicway    APPleway ayssay 'elloha,y' otay #$% Inky2tay-Inky3way onway ishay way otay stairway otay airway."),
                pigLatinService.transformToPigLatin("Epic    APPle says 'hell,o' to #$% Tinky2-Winky3 on his way to stairway to airway."));
    }
}